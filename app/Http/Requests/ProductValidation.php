<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Validation\Rule;
use App\Http\Requests\Api\FormRequest;

class ProductValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $skuRule = Rule::unique((new Product)->getTable());
        if (request()->isMethod('put')) {
            $skuRule->ignore($this->route('product'));
        }
        return [
            'sku' => ['required', 'string', 'max:64', $skuRule],
            'name' => 'required|string|max:128',
            'description' => 'max:256',
        ];
    }
}
