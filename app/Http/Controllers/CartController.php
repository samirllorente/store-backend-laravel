<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cart = Cart::create();
        $products = $cart->products()->attach($request->products);
        // return Cart::with('cart_product')->get()->find($cart->id);
        $cart->products = $cart->products()->get();
        return $cart;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cart = Cart::find($id);
        if($cart) {
            $cart->products = $cart->cart_product()->with('product')->get();
            return $cart;
        }
        return response(['error' => ['code' => 404, 'message' => 'Cart not found']], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Cart::find($id);
        if($cart) {
            if($request->has('state')) {
                $cart->fill(['state' => $request->state]);
                $cart->save();
            }
            if($request->has('products')) {
                $cart->products()->sync($request->products);
            }
            $cart->products = $cart->cart_product()->with('product')->get();
            return $cart;
        }
        return response(['error' => ['code' => 404, 'message' => 'Cart not found']], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }

    public function pay(Request $request, $id) {
        $cart = Cart::find($id);
        if($cart) {
            $request->state = 'completed';
            return $this->update($request, $id);
        }
        return response(['error' => ['code' => 404, 'message' => 'Cart not found']], 404);
    }
}
