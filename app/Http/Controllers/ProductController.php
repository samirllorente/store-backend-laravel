<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\ProductValidation;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qb = Product::query();
        if($request->has('q')){
            $qb->ofSearch($request->get('q'));
        }
        if($request->has('sortBy')){
            $qb->orderBy($request->get('sortBy'), $request->get('direction', 'ASC'));
        }
        return $qb->paginate($request->get('limit', 10));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductValidation $request)
    {
        return Product::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        if($product) {
            return $product;
        }
        return response(['error' => ['code' => 404, 'message' => 'Product not found']], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductValidation $request, $id)
    {
        $product = Product::find($id);
        if($product) {
            $product->fill($request->all());
            $product->save();
            return $product;
        }
        return response(['error' => ['code' => 404, 'message' => 'Product not found']], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product) {
            $product->delete();
            return $product;
        }
        return response(['error' => ['code' => 404, 'message' => 'Product not found']], 404);
    }
}
