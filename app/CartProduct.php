<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $table = 'cart_product';

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
