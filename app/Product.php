<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'sku', 'name', 'description'
    ];

    public function scopeOfSearch($query, $q)
    {
        if ( $q ) {
            $query->orWhere('sku', 'LIKE', '%' . $q . '%')
                ->orWhere('name', 'LIKE', '%' . $q . '%')
                ->orWhere('description', 'LIKE', '%' . $q . '%');
        }
        return $query;
    }
}
