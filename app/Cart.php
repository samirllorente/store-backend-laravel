<?php

namespace App;

use App\Product;
use App\CartProduct;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'state'
    ];

    /**
     * Get the products for the cart.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }

    public function cart_product()
    {
        return $this->hasMany(CartProduct::class);
    }
}
