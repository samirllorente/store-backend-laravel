<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('product', 'ProductController')->except([
    'create', 'edit'
]);
Route::post('cart/pay/{id}', 'CartController@pay');
Route::resource('cart', 'CartController')->only([
    'store', 'show', 'update'
]);